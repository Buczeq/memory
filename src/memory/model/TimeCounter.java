package memory.model;

import javafx.animation.Animation;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.scene.control.Label;
import javafx.util.Duration;

import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;

public class TimeCounter extends Label {
    private static DateTimeFormatter SHORT_TIME_FORMATTER = DateTimeFormatter.ofPattern("HH:mm:ss");
    private final LocalTime start = LocalTime.now();
    private long elapsedTime;

    public TimeCounter() {
        bindToTime();
    }

    private void bindToTime() {
        Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0),
                event -> {
             elapsedTime = start.until(LocalTime.now(),ChronoUnit.SECONDS);
                    setText(LocalTime.ofSecondOfDay(elapsedTime).format(SHORT_TIME_FORMATTER));
                }),
                new KeyFrame(Duration.seconds(1)));

        timeline.setCycleCount(Animation.INDEFINITE);
        timeline.play();
    }

    public long getElapsedTime() {
        return elapsedTime;
    }
}
