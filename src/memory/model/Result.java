package memory.model;

import java.io.Serial;
import java.io.Serializable;
import java.time.LocalTime;

public class Result implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    private String name;
    private LocalTime time;
    private int sizeX;
    private int sizeY;
    private int points;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public LocalTime getTime() {
        return time;
    }

    public void setTime(LocalTime time) {
        this.time = time;
    }

    public String getSize(){
        return sizeX + "x" + sizeY;
    }

    public int getSizeX() {
        return sizeX;
    }

    public void setSizeX(int sizeX) {
        this.sizeX = sizeX;
    }

    public int getSizeY() {
        return sizeY;
    }

    public void setSizeY(int sizeY) {
        this.sizeY = sizeY;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }
}
