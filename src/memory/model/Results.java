package memory.model;

import java.io.Serial;
import java.io.Serializable;
import java.util.ArrayList;

public class Results implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;
    private ArrayList<Result> results = new ArrayList<>();

    public ArrayList<Result> getResults() {
        return results;
    }


}
