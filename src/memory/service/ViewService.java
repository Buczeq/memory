package memory.service;

import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.stage.Window;
import memory.controller.ExceptionController;
import memory.controller.GameController;
import memory.controller.RankController;
import memory.model.Result;

import java.io.IOException;
import java.util.Objects;

public class ViewService {

    public static void showGame(int sizeX, int sizeY) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(ViewService.class.getResource("../view/game.fxml")));

            GameController controller = new GameController();
            controller.setSize(sizeX, sizeY);
            fxmlLoader.setController(controller);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            controller.setupListener(scene);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());

            Stage stage = new Stage();
            stage.setTitle("Game");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ViewService.showException("Błąd podczas inicjalizacji okna. Spróbuj ponownie uruchomić grę.");
        }
    }

    public static void showMainMenu() {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(ViewService.class.getResource("../view/main.fxml")));


            Scene scene = new Scene(root);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());
            Stage stage = new Stage();
            stage.setTitle("Memory The Game");
            stage.setScene(scene);


            stage.show();
            Platform.runLater(root::requestFocus);
        } catch (IOException ex) {
            ViewService.showException("Błąd podczas inicjalizacji okna. Spróbuj ponownie uruchomić grę.");
        }
    }

    public static void showRank(Result result, Window windowToHide) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(ViewService.class.getResource("../view/rank.fxml")));

            RankController controller = new RankController();
            controller.setWindowToHide(windowToHide);
            controller.setResult(result);
            fxmlLoader.setController(controller);

            Parent root = null;
            root = fxmlLoader.load();
            Scene scene = new Scene(root);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());

            Stage stage = new Stage();
            stage.setTitle("Rank");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ViewService.showException("Błąd podczas inicjalizacji okna. Spróbuj ponownie uruchomić grę.");
        }
    }

    public static void showNewGame() {
        try {
            Parent root = FXMLLoader.load(Objects.requireNonNull(ViewService.class.getResource("../view/newGame.fxml")));

            Scene scene = new Scene(root);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());

            Stage stage = new Stage();
            stage.setTitle("New game");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ViewService.showException("Błąd podczas inicjalizacji okna. Spróbuj ponownie uruchomić grę.");
        }
    }

    public static void showHighScores(){
        try {
            Parent root;
            root = FXMLLoader.load(Objects.requireNonNull(ViewService.class.getResource("../view/highScores.fxml")));

            Scene scene = new Scene(root);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());
            Stage stage = new Stage();
            stage.setTitle("High Scores");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            ViewService.showException("Błąd podczas inicjalizacji okna. Spróbuj ponownie uruchomić grę.");
        }
    }

    public static void showException(String message) {
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(Objects.requireNonNull(ViewService.class.getResource("../view/exception.fxml")));

            ExceptionController controller = new ExceptionController();
            fxmlLoader.setController(controller);
            Parent root = fxmlLoader.load();
            Scene scene = new Scene(root);
            controller.setupException(message);
            scene.getStylesheets().add(Objects.requireNonNull(ViewService.class.getResource("../style/styles.css")).toExternalForm());

            Stage stage = new Stage();
            stage.setTitle("Błąd");
            stage.setScene(scene);
            stage.show();
        } catch (IOException ex) {
            System.err.println("Błąd podczas ładowania widoku. sprawdź ścieżki do plików.");
        }
    }

}
