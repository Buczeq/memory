package memory.service;

import javafx.scene.effect.DropShadow;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import memory.model.Card;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class GameService {
    public List<Card> generateCards(int x, int y) {
        int amountOfAll = x * y;
        ArrayList<Card> cards = new ArrayList<>();
        ArrayList<String> images = new ArrayList<>(Arrays.asList("ceylon", "clojure", "fantom", "groovy", "java", "kawa", "kotlin", "ringojs", "scala", "xtend"));


        for (int i = 0; i < amountOfAll / 2; i++) {
            Card card = new Card();
            card.setDisplayed(false);
            card.setImage(images.get(i) + ".png");
            card.setValue(images.get(i));
            card.setEffect(new DropShadow());
            cards.add(card);
            Card card2 = new Card();
            card2.setDisplayed(false);
            card2.setImage(images.get(i) + ".png");
            card2.setValue(images.get(i));
            card2.setEffect(new DropShadow());
            cards.add(card2);
        }
        Collections.shuffle(cards);
        return cards;
    }

    public static void showImage(Card card) {

        try {
            Image image = new Image(new FileInputStream("src/resources/" + card.getImage()));

            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(120);
            imageView.setFitWidth(120);
            imageView.setPreserveRatio(true);

            card.setGraphic(imageView);
            card.setMinHeight(150);
            card.setMinWidth(150);
            card.setMaxWidth(150);
            card.setMaxHeight(150);
        } catch (FileNotFoundException e) {
            ViewService.showException("Błąd podczas ładowania zdjęcia. Upewnij się że masz wszystkie wymagane pliki.");
        }
    }

    public static void hideImage(Card card) {
        try {
            Image image = new Image(new FileInputStream("src/resources/matriximg.png"));

            ImageView imageView = new ImageView(image);
            imageView.setFitHeight(150);
            imageView.setFitWidth(150);
            imageView.setPreserveRatio(true);

            card.setGraphic(imageView);
            card.setMinHeight(150);
            card.setMinWidth(150);
            card.setMaxWidth(150);
            card.setMaxHeight(150);
        } catch (FileNotFoundException e) {
            ViewService.showException("Błąd podczas ładowania zdjęcia. Upewnij się że masz wszystkie wymagane pliki.");
        }
    }




}
