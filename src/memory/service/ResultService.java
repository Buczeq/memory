package memory.service;

import memory.model.Result;
import memory.model.Results;

import java.io.*;

public class ResultService {

    public static void saveResults(Result result) {
        try {
            FileInputStream fi = new FileInputStream("results.txt");
            ObjectInputStream oi = new ObjectInputStream(fi);
            Results oldResults = (Results) oi.readObject();

            oi.close();
            fi.close();
            oldResults.getResults().add(result);
            FileOutputStream f = new FileOutputStream("results.txt");
            ObjectOutputStream o = new ObjectOutputStream(f);

            o.writeObject(oldResults);

            o.close();
            f.close();
        } catch (FileNotFoundException ex) {
            try {
                Results results = new Results();
                results.getResults().add(result);
                FileOutputStream f = new FileOutputStream("results.txt");
                ObjectOutputStream o = new ObjectOutputStream(f);

                o.writeObject(results);


                o.close();
                f.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        } catch (IOException | ClassNotFoundException ioException) {
            ioException.printStackTrace();
        }
    }

    public static Results readResults(){
        try {
            FileInputStream fi = new FileInputStream("results.txt");
            ObjectInputStream oi = new ObjectInputStream(fi);
            Results results = (Results) oi.readObject();

            oi.close();
            fi.close();
            return results;
        }catch (IOException | ClassNotFoundException ex){
            return new Results();
        }
    }
}
