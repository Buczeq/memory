package memory;

import javafx.application.Application;
import javafx.stage.Stage;
import memory.service.ViewService;


public class Main extends Application {

    @Override
    public void start(Stage stage) {
        ViewService.showMainMenu();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
