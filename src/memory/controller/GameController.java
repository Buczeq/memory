package memory.controller;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.geometry.Insets;
import javafx.scene.Scene;
import javafx.scene.effect.BlurType;
import javafx.scene.effect.DropShadow;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.util.Duration;
import memory.model.Card;
import memory.model.Result;
import memory.model.TimeCounter;
import memory.service.GameService;
import memory.service.ViewService;

import java.time.LocalTime;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class GameController {

    @FXML
    private GridPane grid;
    private int sizeX;
    private int sizeY;
    GameService service = new GameService();

    @FXML
    private TimeCounter timeCounter;

    private Card displayedCard;

    @FXML
    public void initialize() {


        List<Card> cards = service.generateCards(sizeX, sizeY);
        List<Card> cardsToAdd = new ArrayList<>(cards);
        grid.setHgap(15);
        grid.setVgap(15);
        grid.setPadding(new Insets(15, 15, 15, 15));
        AtomicInteger displayedToHide = new AtomicInteger();
        for (int i = 0; i < sizeY; i++) {
            for (int j = 0; j < sizeX; j++) {
                Card card = cardsToAdd.get(0);
                grid.add(card, j, i);
                GameService.hideImage(card);

                card.setOnAction(event -> {
                    if(card.isGuessed() || card.isDisplayed() || displayedToHide.get() > 1) return;

                    if(displayedCard == null){
                        card.setDisplayed(true);
                        displayedCard = card;
                        GameService.showImage(card);
                        displayedToHide.getAndIncrement();
                    }else  {
                        card.setDisplayed(true);
                        GameService.showImage(card);
                        displayedToHide.getAndIncrement();
                        Card cardToHiglight1 = card;
                        Card cardToHiglight2 = displayedCard;
                        if(!displayedCard.getValue().equals(card.getValue())){
                            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(2), event1 -> {
                                cardToHiglight1.setEffect(new DropShadow());
                                cardToHiglight2.setEffect(new DropShadow());

                                displayedCard.setDisplayed(false);
                                card.setDisplayed(false);

                                displayedCard.setDirty(true);
                                card.setDirty(true);

                                GameService.hideImage(displayedCard);
                                GameService.hideImage(card);
                                card.setEffect(new DropShadow());
                                displayedCard.setEffect(new DropShadow());
                                displayedCard = null;

                                displayedToHide.set(0);
                            }),new KeyFrame(Duration.seconds(0), event2 -> {
                                DropShadow shadow = new DropShadow();
                                shadow.setColor(Color.RED);
                                shadow.setRadius(20);
                                shadow.setSpread(0.5);
                                shadow.setBlurType(BlurType.GAUSSIAN);
                                cardToHiglight1.setEffect(shadow);
                                cardToHiglight2.setEffect(shadow);
                            }));
                            timeline.play();
                        }else {
                            card.setGuessed(true);
                            displayedCard.setGuessed(true);
                            Timeline timeline = new Timeline(new KeyFrame(Duration.seconds(0), event1 -> {
                                DropShadow shadow = new DropShadow();
                                shadow.setColor(Color.LIGHTGREEN);
                                shadow.setRadius(20);
                                shadow.setSpread(0.5);
                                shadow.setBlurType(BlurType.GAUSSIAN);
                                cardToHiglight1.setEffect(shadow);
                                cardToHiglight2.setEffect(shadow);
                            }), new KeyFrame(Duration.millis(2000), event1 -> {
                                cardToHiglight1.setEffect(new DropShadow());
                                cardToHiglight2.setEffect(new DropShadow());
                            }));
                            timeline.play();
                            displayedCard = null;

                            displayedToHide.set(0);
                            if (cards.stream().filter(Card::isGuessed).collect(Collectors.toList()).containsAll(cards)) {
                                long guessedWithoutDisplaying = cards.stream().filter(card1 -> !card1.isDirty()).count();

                                Result result = new Result();
                                result.setSizeX(sizeX);
                                result.setSizeY(sizeY);
                                result.setTime(LocalTime.ofSecondOfDay(timeCounter.getElapsedTime()));
                                result.setPoints((int)
                                        (((sizeX * sizeX * sizeY * sizeY) +
                                                (guessedWithoutDisplaying * guessedWithoutDisplaying))
                                                / timeCounter.getElapsedTime() + 1));

                                ViewService.showRank(result, grid.getScene().getWindow());
                            }
                        }
                    }
                });
                cardsToAdd.remove(0);
            }
        }

    }


    @FXML
    public void setupListener(Scene scene) {
        scene.addEventFilter(KeyEvent.KEY_PRESSED, new EventHandler<>() {
            final KeyCombination keyComb = new KeyCodeCombination(KeyCode.Q,
                    KeyCombination.CONTROL_DOWN, KeyCombination.SHIFT_DOWN);

            @Override
            public void handle(KeyEvent keyEvent) {
                if (keyComb.match(keyEvent)) {
                    grid.getScene().getWindow().hide();
                    ViewService.showMainMenu();
                    keyEvent.consume();
                }
            }
        });
    }


    public void setSize(int sizeX, int sizeY) {
        this.sizeX = sizeX;
        this.sizeY = sizeY;
    }
}
