package memory.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import memory.service.ViewService;



public class MenuController {

    @FXML
    private Button newGame;

    @FXML
    private Button highScores;

    @FXML
    private Button exit;

    @FXML
    private void newGame() {
        ViewService.showNewGame();
        newGame.getScene().getWindow().hide();
    }

    @FXML
    private void highScores() {
        ViewService.showHighScores();
    }

    @FXML
    private void exit() {
        Platform.exit();
        System.exit(0);
    }
}
