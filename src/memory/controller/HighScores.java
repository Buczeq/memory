package memory.controller;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import memory.model.Result;
import memory.service.ResultService;

import java.util.ArrayList;
import java.util.Comparator;


public class HighScores {

    @FXML
    private TableView<Result> table;

    @FXML
    private void initialize()  {

        TableColumn<Result, String> column1 = new TableColumn<>("Name");
        column1.setCellValueFactory(new PropertyValueFactory<>("name"));

        TableColumn<Result, String> column2 = new TableColumn<>("Time");
        column2.setCellValueFactory(new PropertyValueFactory<>("time"));

        TableColumn<Result, String> column3 = new TableColumn<>("Size");
        column3.setCellValueFactory(new PropertyValueFactory<>("size"));

        TableColumn<Result, String> column4 = new TableColumn<>("Points");
        column4.setCellValueFactory(new PropertyValueFactory<>("points"));

        column1.setMinWidth(540);
        column2.setMinWidth(180);
        column3.setMinWidth(180);
        column4.setMinWidth(180);

        table.getColumns().add(column1);
        table.getColumns().add(column2);
        table.getColumns().add(column3);
        table.getColumns().add(column4);
        ArrayList<Result> results = ResultService.readResults().getResults();
        results.sort(Comparator.comparing(Result::getPoints).reversed());
        table.getItems().addAll(results);

    }
}
