package memory.controller;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import memory.service.ViewService;


public class NewGameController {

    @FXML
    private Button play;

    @FXML
    private TextField gridSizeX;
    @FXML
    private TextField gridSizeY;

    @FXML
    private void startNewGame()  {

        int sizeX = Integer.parseInt(gridSizeX.getText());
        int sizeY = Integer.parseInt(gridSizeY.getText());
        if ((sizeX * sizeY) % 2 != 0) {
            gridSizeX.setText("");
            gridSizeY.setText("");

            Platform.runLater(() -> play.getParent().requestFocus());

            ViewService.showException("Sumaryczna ilość kart musi być parzysta. Zmień rozmiar planszy.");
        }else {
            ViewService.showGame(sizeX, sizeY);
            play.getScene().getWindow().hide();
        }
    }


    @FXML
    private void initialize() {
        Platform.runLater(() -> play.getParent().requestFocus());
        filterInputForGridSize(gridSizeX);
        filterInputForGridSize(gridSizeY);

        play.disableProperty().bind(
                gridSizeX.textProperty().isEmpty()
                        .or(gridSizeY.textProperty().isEmpty())
        );

    }

    private void filterInputForGridSize(TextField gridSize) {
        gridSize.textProperty().addListener((observable, oldValue, newValue) -> {
            if (!newValue.matches("[1-5]*")) {
                gridSize.setText(newValue.replaceAll("[^[1-5]]", ""));
            }
            if (gridSize.getText().length() > 1) {
                String s = gridSize.getText().substring(1, 2);
                gridSize.setText(s);
            }
        });
    }
}
