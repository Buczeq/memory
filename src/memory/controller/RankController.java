package memory.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Window;
import memory.model.Result;
import memory.service.ResultService;
import memory.service.ViewService;


public class RankController {
    @FXML
    private TextField name;
    @FXML
    private Button ok;
    private Window windowToHide;
    private Result result;

    @FXML
    private void initialize(){
        ok.disableProperty().bind(name.textProperty().isEmpty());
        ok.setOnAction(event -> {
            result.setName(name.textProperty().get());
            ResultService.saveResults(result);

            windowToHide.hide();
            ok.getScene().getWindow().hide();

                ViewService.showMainMenu();
        });
    }

    public void setWindowToHide(Window window) {
        this.windowToHide = window;
    }

    public void setResult(Result result) {
        this.result = result;
    }
}
