package memory.controller;

import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class ExceptionController {

    @FXML
    private Label errorMessage;

    @FXML
    private Button ok;

    @FXML
    private void close() {
        errorMessage.getScene().getWindow().hide();
    }


    public void setupException(String message){
        errorMessage.setText(message);
        ok.setOnAction(event -> close());
    }
}
