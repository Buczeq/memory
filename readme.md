# Gra pamięciowa / Memory game

[PL]
Finalna wersja projektu wykonanego w JavaFx na przedmiot "GUI".

Gra pamięciowa polegająca na zaznaczeniu dwóch takich samych kart.
W przypadku gdy karty są takie same, podświetlają się na zielono i pozostają
odkryte. W przeciwnym wypadku obie się zakrywają.

Została zaimplementowana tabela wyników.

Biblioteki znajdują się w repozytorium, wystarczy zbudować i uruchomić.

<a href="https://imgur.com/a/4OJ5b7P">Zdjęcia</a>



[ENG]
Final version of the project made in JavaFx for the subject "GUI".

A memory game which consists in selecting two identical cards.
When the cards are the same, they glow green and stay
discovered. Otherwise, they both cover up.

The result table has been implemented.

The libraries are in the repository, just Build&Run.

<a href="https://imgur.com/a/4OJ5b7P">Images</a>